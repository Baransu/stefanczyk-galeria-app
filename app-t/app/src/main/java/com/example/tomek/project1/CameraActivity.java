package com.example.tomek.project1;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout frameLayout;

    private String rootFolder = "/TomaszCichocinski/";

    private ImageView cameraOptionsButton1;
    private ImageView cameraOptionsButton2;
    private ImageView cameraOptionsButton3;
    private ImageView cameraOptionsButton4;

    private Camera.Parameters camParams;

    int cameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
    private CameraPreview cameraPreview;

    private int mRectCount = 0;
    private int mCircleRadius = 200;

    private int circleX = 0;
    private int circleY = 0;
    private Circle circle;

    private String[] imageOptions = {"podglad", "usun", "zapisz"};

    private SeekBar cameraZoom;

    private String[] folders;

    private ImageView takePictureButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        initCamera(cameraOrientation);

        cameraPreview = new CameraPreview(CameraActivity.this, camera);
        frameLayout = (FrameLayout)findViewById(R.id.cameraLayoutMain);
        frameLayout.addView(cameraPreview);
        camParams = camera.getParameters();

        takePictureButton = (ImageView)findViewById(R.id.takePictureButton);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePictureButton.setVisibility(View.INVISIBLE);
                camera.takePicture(null, null, camPictureCallback);
            }
        });

        circleX = getWindowManager().getDefaultDisplay().getWidth()/2;
        circleY = getWindowManager().getDefaultDisplay().getHeight()/2 - mCircleRadius/2;
        circle = new Circle(CameraActivity.this, mCircleRadius);
        circle.setVisibility(View.INVISIBLE);
        frameLayout.addView(circle);

        cameraOptionsButton1 = (ImageView)findViewById(R.id.cameraOptionsButton1);
        cameraOptionsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraSizeOptions();
            }
        });

        cameraOptionsButton2 = (ImageView)findViewById(R.id.cameraOptionsButton2);
        cameraOptionsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraBalanceOptions();
            }
        });

        cameraOptionsButton3 = (ImageView)findViewById(R.id.cameraOptionsButton3);
        cameraOptionsButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraExposureOptions();
            }
        });

        cameraZoom = (SeekBar)findViewById(R.id.camera_zoom);
        cameraZoom.setMax(camParams.getMaxZoom());
        cameraZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                camParams.setZoom(val);
                camera.setParameters(camParams);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }

        });

//        cameraOptionsButton4 = (ImageView)findViewById(R.id.cameraOptionsButton4);
//        cameraOptionsButton4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(cameraZoom.getVisibility() == View.INVISIBLE) {
//                    cameraZoom.setVisibility(View.VISIBLE);
//                } else {
//                    cameraZoom.setVisibility(View.INVISIBLE);
//                }
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cameraPreview.getCamera() == null) {
            initCamera(cameraOrientation);
            cameraPreview.setCamera(camera);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraPreview.setCamera(null);
        camera.release();
    }

    private View.OnLongClickListener imgOptionsListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(final View v) {

            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Image");
            alert.setItems(imageOptions, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch(which) {

                        //podglad
                        case 0:

                            ProgressDialog pDialog = new ProgressDialog(CameraActivity.this);
                            pDialog.setMessage("wczytywanie");
                            pDialog.setCancelable(false); // nie da się zamknąć klikając w ekran
                            pDialog.show();

                            File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/camera_temp");
                            FileOutputStream fs = null;
                            try {

                                fs = new FileOutputStream(f);
                                Rect r = (Rect)v;
                                r.getBmp().compress(Bitmap.CompressFormat.PNG, 100, fs);
                                fs.flush();
                                fs.close();

                                Intent intent = new Intent(CameraActivity.this, SingleImageActivity.class);
                                intent.putExtra("path", f.getAbsolutePath());
                                camera.stopPreview();
                                pDialog.dismiss();
                                startActivity(intent);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            break;

                        //usun
                        case 1:

                            Rect r = (Rect)v;
                            frameLayout.removeView(r);
                            mRectCount--;
                            if(mRectCount <= 0) {
                                circle.setVisibility(View.INVISIBLE);
                                updateRects();
                            }

                            break;

                        //zapis
                        case 2:

                            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski");

                            int ssize = file.listFiles().length;
                            Log.d("TC", ""+ssize);
                            ArrayList<String> ffol = new ArrayList<String>();
                            for (int i = 0; i < ssize; i++) {
                                if (file.listFiles()[i].isDirectory()) {
                                    ffol.add(file.listFiles()[i].getName());
                                }
                            }

                            folders = new String[ffol.size()];
                            for(int i = 0; i < ffol.size(); i++) {
                                folders[i] = ffol.get(i);
                            }

                            AlertDialog.Builder aalert = new AlertDialog.Builder(CameraActivity.this);
                            aalert.setTitle("Folder zapisu: ");
                            aalert.setItems(folders, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    final int aaa = which;
                                    AlertDialog.Builder aalert = new AlertDialog.Builder(CameraActivity.this);
                                    aalert.setTitle("Zapis");
                                    aalert.setMessage("Podaj nazwe pliku");
                                    final EditText input = new EditText(CameraActivity.this);
                                    aalert.setView(input);
                                    aalert.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/" + folders[aaa] + "/" + input.getText().toString());
                                            FileOutputStream fs = null;
                                            try {

                                                fs = new FileOutputStream(folder);
                                                Rect r = (Rect)v;
                                                r.getBmp().compress(Bitmap.CompressFormat.PNG, 100, fs);
                                                fs.flush();
                                                fs.close();

                                                frameLayout.removeView(r);
                                                mRectCount--;
                                                if(mRectCount <= 0) {
                                                    circle.setVisibility(View.INVISIBLE);
                                                    updateRects();
                                                }

                                                Toast.makeText(CameraActivity.this, "zapisano w " + folders[aaa], Toast.LENGTH_SHORT).show();

                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    });

//no
                                    aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
//
                                    aalert.show();
                                }
                            });
                            aalert.show();


                            break;
                    }
                }
            });
            alert.show();

            return false;
        }
    };

    private View.OnTouchListener mover = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            FontPreview font = (FontPreview)v;
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:

                    font.setX(event.getRawX() + font.holdModifierX);
                    font.setY(event.getRawY() + font.holdModifierY);

                    break;

                case MotionEvent.ACTION_DOWN:

                    font.holdModifierX = font.getX() - event.getRawX();
                    font.holdModifierY = font.getY() - event.getRawY();

                    break;
                case MotionEvent.ACTION_UP:

                    font.holdModifierX = 0;
                    font.holdModifierY = 0;

                    break;

                default:
                    break;

            }
            return true;
        }
    };

    private void updateRects(){
        int count = frameLayout.getChildCount();
        for(int i = 2; i < count; i++) {

            int angle = 360/mRectCount;
            int x = (int) (circleX + Math.cos(Math.toRadians(angle * i)) * mCircleRadius);
            int y = (int) (circleY + Math.sin(Math.toRadians(angle * i)) * mCircleRadius);

            View child = frameLayout.getChildAt(i);
//            child.setOnTouchListener(mover);
            child.setX(x - 125/2);
            child.setY(y - 125 - 125/2);
        }
    }

    private void cameraSizeOptions() {
        if(camParams != null) {
            final List<Camera.Size> resolutions = camParams.getSupportedPictureSizes();
            String[] options = new String[resolutions.size()];
            for(int i = 0; i < resolutions.size(); i++) {
                options[i] = resolutions.get(i).width + "x" + resolutions.get(i).height;
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Set resolution");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setPictureSize(resolutions.get(which).width, resolutions.get(which).height);
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private void cameraBalanceOptions() {
        if(camParams != null) {
            final List<String> balance = camParams.getSupportedWhiteBalance();
            String[] options = new String[balance.size()];
            for(int i = 0; i < balance.size(); i++) {
                options[i] = balance.get(i);
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Set white balance");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setWhiteBalance(balance.get(which));
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private void cameraExposureOptions() {
        if(camParams != null) {
            int min = camParams.getMinExposureCompensation();
            int max = camParams.getMaxExposureCompensation();
            final int[] options = new int[max + Math.abs(min)];
            String[] opt = new String[max + Math.abs(min)];
            int cur = 0;
            for(int i = min; i < max; i++) {
                options[cur] = i;
                opt[cur++] = "" + i;
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Set exposure");
            alert.setItems(opt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setExposureCompensation(options[which]);
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback () {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            byte[] fdata = data;

//            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
//            String d = dFormat.format(new Date());
//
//            File myPhoto = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + rootFolder + "Zdjecia/" + d);
//            FileOutputStream fs = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeByteArray(fdata, 0, fdata.length, options);
            Bitmap bmp = bitmap;
            switch (getResources().getConfiguration().orientation) {
                //does not work (rotation locked)
                case Configuration.ORIENTATION_LANDSCAPE:
                    bmp = RotateBitmap(bitmap, 90);
                    break;
                //it work
                case Configuration.ORIENTATION_PORTRAIT:
                    bmp = RotateBitmap(bitmap, 90);
                    break;
            }

//                fs = new FileOutputStream(myPhoto);
//                bmp.compress(Bitmap.CompressFormat.PNG, 100, fs);
//                fs.flush();
//                fs.close();

            Bitmap smallBmp = Bitmap.createScaledBitmap(bmp, 125, 125, false);

            Rect rect = new Rect(CameraActivity.this, smallBmp, bmp);
            rect.setOnLongClickListener(imgOptionsListener);
            frameLayout.addView(rect);
            mRectCount++;

            if(mRectCount > 0) {
                circle.setVisibility(View.VISIBLE);
            }


            updateRects();

            takePictureButton.setVisibility(View.VISIBLE);
            camera.startPreview();
        }
    };

    public static Bitmap RotateBitmap(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initCamera(int orientation){
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            Intent intent = new Intent(CameraActivity.this, MainActivity.class);
            Toast.makeText(CameraActivity.this, "Camera not avaliable", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        } else {
            cameraId = getCameraId(orientation);
            if (cameraId < 0) {
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
            } else {
                camera = Camera.open();
            }
        }
    }

    private int getCameraId(int orientation){
        int camerasCount = Camera.getNumberOfCameras();
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            // 0 - back camera
            // 1 - front camera

            if (orientation == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return 0;
            } else if (orientation == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return 1;
            }
        }
        return -1;
    }
}
