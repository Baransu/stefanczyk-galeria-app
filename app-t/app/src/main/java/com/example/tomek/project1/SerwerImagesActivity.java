package com.example.tomek.project1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SerwerImagesActivity extends AppCompatActivity {

    private ArrayList<Bitmap> images = new ArrayList<Bitmap>();
    private ArrayList<String> imagesPaths = new ArrayList<String>();

    private LinearLayout gallery;

    private ProgressDialog pDialog;

    public static Socket socket;
    {
        try {
            socket = IO.socket(Consts.IP_ADRESS);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serwer_images);

        //pobierz intent data
        gallery = (LinearLayout)findViewById(R.id.web_gallery);

        if(MainActivity.isInternet(SerwerImagesActivity.this)) {

            pDialog = new ProgressDialog(SerwerImagesActivity.this);
            pDialog.setMessage("wczytywanie");
            pDialog.setCancelable(false);
            pDialog.show();

            socket.connect();
            socket.emit("images-all-request");
            socket.on("images-all-respond", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    ClientPlayer.runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject data = (JSONObject) args[0];
                            JSONArray imgs;
                            JSONArray imgsData;
                            try {
                                imgs = data.getJSONArray("images");
                                imgsData = data.getJSONArray("imagesData");
                                for(int i = 0; i < imgs.length(); i++) {
                                    imagesPaths.add((String)imgsData.get(i));
                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    byte[] bmp = (byte[]) imgs.get(i);
                                    images.add(BitmapFactory.decodeByteArray(bmp, 0, bmp.length, options));
                                }

                                showImages();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

        } else {
            Toast.makeText(SerwerImagesActivity.this, "brak internetu", Toast.LENGTH_SHORT).show();
        }

    }

    void showImages(){
        Random generator = new Random();
        LinearLayout contener = new LinearLayout(this);
        int index = 1;
        int maxIndex = 3;
        Display display = getWindowManager().getDefaultDisplay();
        for(int i = 0; i < images.size();i++){
            //image
            int weight = generator.nextInt(2) + 1;
            GalleryImageView imageView = new GalleryImageView(SerwerImagesActivity.this, images.get(i), weight, imagesPaths.get(i));
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    GalleryImageView img = (GalleryImageView)v;
                    String link = "" + Consts.IP_ADRESS_HTTP + "/fullsize/" + img.path;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);

                }
            });
            //content
            LinearLayout.LayoutParams contenerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, generator.nextInt(150) + 200);
            contener.setLayoutParams(contenerParams);
            if(index == maxIndex){
                contener.addView(imageView);
                gallery.addView(contener);
                contener = new LinearLayout(SerwerImagesActivity.this);
                maxIndex = generator.nextInt(3) + 1;
                index = 0;
            }
            else if(i == images.size() - 1){
                contener.addView(imageView);
                gallery.addView(contener);
            }
            else{
                contener.addView(imageView);
            }
            index++;
        }
        pDialog.dismiss();
    }

}
