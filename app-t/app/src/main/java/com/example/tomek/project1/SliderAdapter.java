package com.example.tomek.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by tomek on 09.12.15.
 */
public class SliderAdapter extends FragmentPagerAdapter{

    private ArrayList<SliderFragment> mSliderFragment;
    public ArrayList<Bitmap> bmps;

    public SliderAdapter(FragmentManager fm, ArrayList<SliderFragment> mSliderFragment, ArrayList<Bitmap> bmps) {
        super(fm);
        this.bmps = bmps;
        this.mSliderFragment = mSliderFragment;
    }

    @Override
    public Fragment getItem(int position) {
        MainActivity.uImg(position);

        Fragment fragment = mSliderFragment.get(position);
        Bundle args = new Bundle();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmps.get(position).compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] compressedByteArray = stream.toByteArray();
        Log.d("TC", "slider_adapter: "+compressedByteArray);
        args.putSerializable("img", compressedByteArray);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getItemPosition(Object object) {

        SliderFragment f = (SliderFragment ) object;
        if (f != null) {
            int position = mSliderFragment.indexOf(object);
            f.update(bmps.get(position));
        }

        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return mSliderFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ""+position;
    }
}
