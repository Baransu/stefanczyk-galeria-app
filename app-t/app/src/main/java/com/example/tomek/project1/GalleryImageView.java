package com.example.tomek.project1;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by Tomek on 16.09.2015.
 */
public class GalleryImageView extends ImageView {

    public String path;

    public GalleryImageView(Context context, Bitmap bmp, int weight, String path){
        super(context);
        this.setImageBitmap(bmp);
        this.path = path;
        this.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = weight;
        this.setLayoutParams(params);
    }
}
