package com.example.tomek.project1;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class EditActivity extends AppCompatActivity {

    private String path;
    private ImageView imageView;
    private ImageView.ScaleType[] scaleTypes = {ImageView.ScaleType.FIT_CENTER, ImageView.ScaleType.CENTER, ImageView.ScaleType.CENTER_CROP};

    private RelativeLayout mainLayout;

    private int clickCount = 0;

    private ImageView fontEditor;
    private ImageView flipImage;
    private ImageView rotateImage;
    private ImageView colorFilter;
    private ImageView seekControl;
    private ImageView upload;
    private ImageView save;

    private Bitmap bitmap;
    private Bitmap originalBitmap;

    private ImageView cropToggle;
    private LinearLayout cropControlls;
    private ImageView cropAccept;
    private ImageView cropDecline;

    private RelativeLayout cropper;
    private LinearLayout cropperResizer;

    private int croppModifierX = 0;
    private int croppModifierY = 0;

    private ArrayList<FontPreview> addedFonts = new ArrayList<FontPreview>();

    private float[][] rotationArray = new float[][]{
            { -1.0f, 1.0f},
            { 1.0f, -1.0f},
            { -1.0f, 1.0f},
            { -1.0f, -1.0f}
    };

    private int currentFlip = 0;

    private float[][] effectsArray = new float[][] {
            {
                    1, 0, 0, 0, 0,
                    0, 1, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            },
            {
                    0.299f, 0.299f, 0.299f, 0, 0,
                    0.587f, 0.587f, 0.587f, 0, 0,
                    0.114f, 0.114f, 0.114f, 0, 0,
                    0.000f, 0.000f, 0.000f, 1, 0

            }
    };

    private String[] effectNames = {
            "normal",
            "czerwony",
            "negatyw",
            "zielony"
    };

    private LinearLayout seekLayout;
    private SeekBar contrastBar;
    private SeekBar brightnessBar;
    private SeekBar saturationBar;

    private ProgressDialog pDialog;

    private String[] folders;
    private String[] folderPaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");

        imageView = (ImageView)findViewById(R.id.singleImageView);

        mainLayout = (RelativeLayout)findViewById(R.id.edit_font);

        originalBitmap = bitmap = Tools.decodeImage(path, 2);
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(scaleTypes[clickCount]);

        cropControlls = (LinearLayout) findViewById(R.id.cropControlls);

        cropper = (RelativeLayout) findViewById(R.id.cropper);
        mainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:

//
//                        int width = mainLayout.getWidth();
//                        int height = mainLayout.getHeight();


//                        cropper.setX(event.getRawX() + croppModifierX);
//                        cropper.setY(event.getRawY() + croppModifierY);

                        int w = (int)(event.getRawX() - cropper.getX());
                        int h = (int)(event.getRawY() - cropper.getY());

                        if(w < 10) w = 10;
                        if(h < 10) h = 10;

                        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(w,h);
                        cropper.setLayoutParams(rel_btn);
//
//                        cropper.getLayoutParams().width = (int) ();
//                        cropper.getLayoutParams().height = (int) (event.getRawY() - cropper.getY());

                        break;
                    case MotionEvent.ACTION_DOWN:

//                        cropper.setVisibility(View.VISIBLE);

                        cropper.setX(event.getRawX());
                        cropper.setY(event.getRawY());

//                        cropper.getLayoutParams().width = 1;
//                        cropper.getLayoutParams().height = 1;

                        break;
                    case MotionEvent.ACTION_UP:
//                        croppModifierX = 0;
//                        croppModifierY = 0;
                        break;
                    default:
                        break;
                }
                return true;
            }
        });


//        cropperResizer = (LinearLayout) findViewById(R.id.cropperResizer);
//        cropperResizer.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_MOVE:
//
//                        cropper.setX(event.getRawX() + croppModifierX);
//
//                        int width = (int) (cropper.getX() + cropper.getWidth() - event.getX());
//                        int height = (int) (cropper.getY() + cropper.getHeight() - event.getY());
//
//                        cropper.getLayoutParams().width = cropper.getWidth() - width;
//                        cropper.getLayoutParams().height = cropper.getHeight() - height;
////                        if(cropper.getX() < 0) {
////
////                            cropper.setX(0);
////                        } else if( cropper.getWidth() + cropper.getX() > width) {
////                            cropper.setX(width - cropper.getWidth());
////                        }
//
//                        cropper.setY(event.getRawY() + croppModifierY);
//
////                        if(cropper.getY() < 0) {
////                            cropper.setY(0);
////                        } else if( cropper.getHeight() + cropper.getY() > height) {
////                            cropper.setY(height - cropper.getHeight());
////                        }
//
//                        break;
//                    case MotionEvent.ACTION_DOWN:
////                        croppModifierX = (int) (cropper.getX() - event.getRawX());
////                        croppModifierY = (int) (cropper.getY() - event.getRawY());
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        v.getParent().requestDisallowInterceptTouchEvent(false);
//
////                        croppModifierX = 0;
////                        croppModifierY = 0;
//                        break;
//                    default:
//                        break;
//                }
//                return true;
//            }
//        });

        cropToggle = (ImageView) findViewById(R.id.singleImage_cropToggle);
        cropToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropControlls.setVisibility(View.VISIBLE);
                if(seekLayout.getVisibility() == View.VISIBLE){
                    seekLayout.setVisibility(View.INVISIBLE);
                }

                //show cropper
                cropper.setVisibility(View.VISIBLE);
            }
        });

        cropAccept = (ImageView) findViewById(R.id.cropAccept);
        cropAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropControlls.setVisibility(View.INVISIBLE);
                //hide
                cropper.setVisibility(View.INVISIBLE);
                //cropp
                cropp();

                RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(0,0);
                cropper.setLayoutParams(rel_btn);

                cropper.setX(0);
                cropper.setY(0);
            }
        });

        cropDecline = (ImageView) findViewById(R.id.cropDecline);
        cropDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropControlls.setVisibility(View.INVISIBLE);
                //hide cropper
                cropper.setVisibility(View.INVISIBLE);

                RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(0,0);
                cropper.setLayoutParams(rel_btn);

                cropper.setX(0);
                cropper.setY(0);
            }
        });


        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski");

        int size = file.listFiles().length;
        Log.d("TC", ""+size);
        ArrayList<String> fol = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            if (file.listFiles()[i].isDirectory()) {
                fol.add(file.listFiles()[i].getName());
            }
        }

        folders = new String[fol.size()];
        for(int i = 0; i < fol.size(); i++) {
            folders[i] = fol.get(i);
        }

        save = (ImageView)findViewById(R.id.singleImage_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] options = {"zapisz", "zapisz z tekstem"};

                AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                alert.setTitle("Kolaz");
                alert.setItems(options, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
//                    Log.d("TC", ""+which);

                                switch (which) {
                                    case 0:

                                        AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                                        alert.setTitle("Save folder: ");
                                        alert.setItems(folders, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                final int aaa = which;
                                                AlertDialog.Builder aalert = new AlertDialog.Builder(EditActivity.this);
                                                aalert.setTitle("Zapis");
                                                aalert.setMessage("Podaj nazwe pliku");
                                                final EditText input = new EditText(EditActivity.this);
                                                aalert.setView(input);
                                                aalert.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/" + folders[aaa] + "/" + input.getText().toString());
                                                        FileOutputStream fs = null;
                                                        try {

                                                            fs = new FileOutputStream(folder);
                                                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fs);
                                                            fs.flush();
                                                            fs.close();

                                                            Toast.makeText(EditActivity.this, "zapisano w " + folders[aaa], Toast.LENGTH_SHORT).show();

                                                        } catch (FileNotFoundException e) {
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                });

//no
                                                aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                });
//
                                                aalert.show();
                                            }
                                        });
                                        alert.show();


                                        break;
                                    case 1:

                                        AlertDialog.Builder balert = new AlertDialog.Builder(EditActivity.this);
                                        balert.setTitle("Save folder: ");
                                        balert.setItems(folders, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                final int aaa = which;
                                                AlertDialog.Builder aalert = new AlertDialog.Builder(EditActivity.this);
                                                aalert.setTitle("Zapis");
                                                aalert.setMessage("Podaj nazwe pliku");
                                                final EditText input = new EditText(EditActivity.this);
                                                aalert.setView(input);
                                                aalert.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/" + folders[aaa] + "/" + input.getText().toString());
                                                        FileOutputStream fs = null;
                                                        try {

                                                            // Get image matrix values and place them in an array
                                                            float[] f = new float[9];
                                                            imageView.getImageMatrix().getValues(f);

                                                            // Extract the scale values using the constants (if aspect ratio maintained, scaleX == scaleY)
                                                            float scaleX = f[Matrix.MSCALE_X];
                                                            float scaleY = f[Matrix.MSCALE_Y];

                                                            // Get the drawable (could also get the bitmap behind the drawable and getWidth/getHeight)
                                                            Drawable d = imageView.getDrawable();
                                                            int origW = d.getIntrinsicWidth();
                                                            int origH = d.getIntrinsicHeight();

                                                            // Calculate the actual dimensions
                                                            int imgWidth = Math.round(origW * scaleX);
                                                            int imgHeight = Math.round(origH * scaleY);

                                                            int viewWidth = imageView.getWidth();
                                                            int viewHeight = imageView.getHeight();

                                                            Log.d("TC", imgWidth + " x " + imgHeight);
                                                            Log.d("TC", viewWidth + " x " + viewHeight);

                                                            int x = (viewWidth - imgWidth)/2;
                                                            int y = (viewHeight - imgHeight)/2;

                                                            Bitmap bmp1 = Tools.bitmapFromView(mainLayout);

                                                            Bitmap bmp2 = Tools.editBitmapFromView(bmp1, x, y, imgWidth, imgHeight);

                                                            fs = new FileOutputStream(folder);
                                                            bmp2.compress(Bitmap.CompressFormat.PNG, 100, fs);
                                                            fs.flush();
                                                            fs.close();

                                                            Toast.makeText(EditActivity.this, "zapisano w " + folders[aaa], Toast.LENGTH_SHORT).show();


                                                        } catch (FileNotFoundException e) {
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                });

//no
                                                aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                });
//
                                                aalert.show();
                                            }
                                        });
                                        balert.show();

                                        break;
                                }

                            }
                        });
                alert.show();

            }
        });


        //font editor
        fontEditor = (ImageView)findViewById(R.id.singleImage_fontEditor);
        fontEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TC", "fontEditor");
                Intent pickContactIntent = new Intent(EditActivity.this, FontActivity.class);
                startActivityForResult(pickContactIntent, 777);
            }
        });

        //flip image
        flipImage = (ImageView)findViewById(R.id.singleImage_flipImage);
        flipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.flipBitmap(bitmap, rotationArray[currentFlip]);
                originalBitmap = Tools.flipBitmap(originalBitmap, rotationArray[currentFlip]);
                currentFlip++;
                if (currentFlip == rotationArray.length)
                    currentFlip = 0;
                imageView.setImageBitmap(bitmap);
            }
        });

        //rotate
        rotateImage = (ImageView)findViewById(R.id.singleImage_rotateImage);
        rotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.rotateBitmap(bitmap);
                originalBitmap = Tools.rotateBitmap(originalBitmap);
                imageView.setImageBitmap(bitmap);
            }
        });

        colorFilter = (ImageView)findViewById(R.id.singleImage_colorFilter);
        colorFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                alert.setTitle("Image effect");
                alert.setItems(effectNames, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            bitmap = originalBitmap;
                        } else {
                            bitmap = Tools.changeColor(bitmap, effectsArray[which]);
                        }
                        imageView.setImageBitmap(bitmap);

                    }
                });
                alert.show();
            }
        });

        seekLayout = (LinearLayout)findViewById(R.id.singleImage_seekLayout);
        seekLayout.setVisibility(View.INVISIBLE);

        contrastBar = (SeekBar)findViewById(R.id.singleImage_contrast);
        contrastBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeContrast(originalBitmap, contrastBar.getProgress(), brightnessBar.getProgress() - 255);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });

        brightnessBar = (SeekBar)findViewById(R.id.singleImage_brigthness);
        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeBrightness(originalBitmap, brightnessBar.getProgress() - 255, contrastBar.getProgress());
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });

        saturationBar = (SeekBar)findViewById(R.id.singleImage_saturation);
        saturationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeSaturation(originalBitmap, saturationBar.getProgress() - 100);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });

        //rotate
        seekControl = (ImageView)findViewById(R.id.singleImage_seekControl);
        seekControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(seekLayout.getVisibility() == View.INVISIBLE) {
                    seekLayout.setVisibility(View.VISIBLE);
                } else {
                    seekLayout.setVisibility(View.INVISIBLE);
                }
            }
        });

        upload = (ImageView)findViewById(R.id.singleImage_upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] opt = {"Upload na serwer", "Share"};
                AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                alert.setTitle("Upload");
                alert.setItems(opt, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which) {
                            //upload na serwer
                            case 0:

                            if(isInternet()) {
                                pDialog = new ProgressDialog(EditActivity.this);
                                pDialog.setMessage("zapisywanie");
                                pDialog.setCancelable(false); // nie da się zamknąć klikając w ekran
                                pDialog.show();

                                Socket socket;
                                {
                                    try {
                                        socket = IO.socket(Consts.IP_ADRESS);
                                    } catch (URISyntaxException e) {
                                        throw new RuntimeException(e);
                                    }
                                }

                                socket.connect();

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] byteArray = stream.toByteArray();

                                Bitmap b = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 5, bitmap.getHeight() / 5, true);
                                stream = new ByteArrayOutputStream();
                                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] compressedByteArray = stream.toByteArray();
                                JSONObject data = new JSONObject();

                                try {
                                    data.put("image", byteArray);
                                    data.put("imageMin", compressedByteArray);
                                    socket.emit("image-save-request", data);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                socket.on("image-save-success", new Emitter.Listener() {
                                    @Override
                                    public void call(final Object... args) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(EditActivity.this, "Wyslano na serwer", Toast.LENGTH_SHORT).show();
                                                pDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                                socket.on("image-save-failed", new Emitter.Listener() {
                                    @Override
                                    public void call(final Object... args) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(EditActivity.this, "Bload uploadu na serwer", Toast.LENGTH_SHORT).show();
//                                                MainActivity.updateList(EditActivity.this);
                                                pDialog.dismiss();
                                            }
                                        });
                                    }
                                });
                            } else {
                                Toast.makeText(EditActivity.this, "Brak internetu", Toast.LENGTH_SHORT).show();
                            }

                            break;
                            //share
                            case 1:

                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.setType("image/jpeg");

                                File myPhoto = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/tmp.jpeg");
                                FileOutputStream fs = null;
                                try {
                                    fs = new FileOutputStream(myPhoto);
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fs);
                                    fs.flush();
                                    fs.close();

//                                    Log.d("TC", myPhoto.toURI().toString());
                                    share.putExtra(Intent.EXTRA_STREAM, Uri.parse(myPhoto.toURI().toString()));
                                    startActivity(Intent.createChooser(share, "Podziel się plikiem!"));

                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                break;
                        }
                    }
                });
                alert.show();
            }
        });
    }

    private void cropp(){
        Bitmap bmp1 = Tools.bitmapFromView(mainLayout);

        int x = (int) cropper.getX();
        int y = (int) cropper.getY();
        int width = cropper.getWidth();
        int height = cropper.getHeight();

        bitmap = Tools.editBitmapFromView(bmp1, x, y, width, height);

        for(FontPreview fp : addedFonts) {
            mainLayout.removeView(fp);
        }

        addedFonts = new ArrayList<FontPreview>();

        imageView.setImageBitmap(bitmap);

    }

    private boolean isInternet(){
        ConnectivityManager connectivityManager = (ConnectivityManager) EditActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }
        else{
            return true;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == 777) {
            Bundle extras = data.getExtras();
            addText((String)extras.get("text"), (String)extras.get("font"), (int)extras.get("color"), (int)extras.get("outline"));
        }
    }

    private void addText(String text, String font, int color, int outline) {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        FontPreview ft = new FontPreview(EditActivity.this, text, typeface, color, outline);
        ft.setOnTouchListener(mover);
        addedFonts.add(ft);
        mainLayout.addView(ft);
        Log.d("TC", "add");
    }

    private View.OnTouchListener mover = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            FontPreview font = (FontPreview)v;
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:

                    font.setX(event.getRawX() + font.holdModifierX);
                    font.setY(event.getRawY() + font.holdModifierY);

                    break;

                case MotionEvent.ACTION_DOWN:

                    font.holdModifierX = font.getX() - event.getRawX();
                    font.holdModifierY = font.getY() - event.getRawY();

                    break;
                case MotionEvent.ACTION_UP:

                    font.holdModifierX = 0;
                    font.holdModifierY = 0;

                    break;

                default:
                    break;

            }
            return true;
        }
    };
}
