package com.example.tomek.project1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Tomek on 04.11.2015.
 */
public class Font extends LinearLayout {

    private TextView text;
    public String font;
    private Typeface tf;

    public Typeface getFont() {
        return tf;
    }

    public Font(Context context, String text, String font, Typeface tf){
        super(context);

        this.font = font;
        this.tf = tf;
        this.font = font;

        //text
        this.text = new TextView(context);
        this.text.setText(text);
        this.text.setTextColor(Color.WHITE);
        this.text.setTextSize(30);
        this.text.setTypeface(this.tf);
        this.text.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
        this.text.setLayoutParams(textParams);

        this.addView(this.text);
    }
}
