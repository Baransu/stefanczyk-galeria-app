package com.example.tomek.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

import java.io.File;

/**
 * Created by Tomek on 23.09.2015.
 */
public class Tools {

    public static Bitmap bitmapFromView(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache();
        Bitmap b = Bitmap.createBitmap( v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    public static Bitmap editBitmapFromView(Bitmap src, int x, int y, int width, int height) {
        Bitmap b = Bitmap.createBitmap(src, x, y, width, height);
        return b;
    }

    public static Bitmap decodeImage(String filePath, int compression) {
        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inDensity = 0;
        options.inSampleSize = compression;
        File imgFile = new File(filePath);
        myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        return myBitmap;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap rotateBitmap(Bitmap bmp) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0,bmp.getWidth(), bmp.getHeight(),matrix, true);

        return rotated;
    }

    public static Bitmap flipBitmap(Bitmap bmp, float[] direction) {
        Matrix matrix = new Matrix();

        matrix.postScale(direction[0], direction[1]);

        Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0,bmp.getWidth(), bmp.getHeight(),matrix, true);

        return rotated;
    }

    public static Bitmap changeColor(Bitmap bmp, float[] color) {
        ColorMatrix cMatrix = new ColorMatrix();

        cMatrix.set(color);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeContrast(Bitmap bmp, float contrast, float brightness) {
        ColorMatrix cMatrix = new ColorMatrix();

        float[] changeMatrix = {
                contrast, 0, 0, 0, brightness,
                0, contrast, 0, 0, brightness,
                0, 0, contrast, 0, brightness,
                0, 0, 0, 1, 0
        };

        cMatrix.set(changeMatrix);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeSaturation(Bitmap bmp, float saturation) {
        ColorMatrix cMatrix = new ColorMatrix();

        cMatrix.setSaturation(saturation);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeBrightness(Bitmap bmp, float brightness, float contrast) {
        ColorMatrix cMatrix = new ColorMatrix();

        float[] changeMatrix = {
                contrast, 0, 0, 0, brightness,
                0, contrast, 0, 0, brightness,
                0, 0, contrast, 0, brightness,
                0, 0, 0, 1, 0
        };

        cMatrix.set(changeMatrix);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }
}
