package com.example.tomek.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by tomek on 09.12.15.
 */
public class SliderFragment extends Fragment {

    private ImageView imageView;
    private Bitmap bitmap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.slider_fragment, container, false);
//        Bundle args = getArguments();

//        byte[] bmp = (byte[])args.getSerializable("img");
//        Log.d("TC", "slider_fragment: "+bmp);
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        bitmap = BitmapFactory.decodeByteArray(bmp, 0, bmp.length, options);

        imageView = (ImageView) rootView.findViewById(R.id.slider_image);
        imageView.setImageBitmap(bitmap);

        return rootView;
    }

    public void update(Bitmap bitmap){
        this.bitmap = bitmap;
        imageView.setImageBitmap(bitmap);
    }
}
