package com.example.tomek.project1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class FileManagerActivity extends AppCompatActivity {

    private LinearLayout content;
    private Display display;
    private String[] fol;

    private ArrayList<String> folders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        display = getWindowManager().getDefaultDisplay();
        Bundle bundle = getIntent().getExtras();
        fol = bundle.getStringArray("folders");

        content = (LinearLayout)findViewById(R.id.content);
        createButtonsForDirectories();

        LinearLayout newFolder = (LinearLayout)findViewById(R.id.aaa);
        newFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder aalert = new AlertDialog.Builder(FileManagerActivity.this);
                aalert.setTitle("Nowy folder");
                aalert.setMessage("Podaj nazwe nowego folderu");
                final EditText input = new EditText(FileManagerActivity.this);
                aalert.setView(input);
                aalert.setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TomaszCichocinski/" + input.getText().toString());
                        if(!folder.exists()) {
                            folder.mkdir();
                            createButtonsForDirectories();
                        }
                    }

                });

                aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) { }
                });

                aalert.show();
            }
        });

    }

    public void createButtonsForDirectories(){


        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TomaszCichocinski/");
        ArrayList<String> folders = new ArrayList<String>();

        for(File f : file.listFiles()) {
            if(f.isDirectory()) {
                folders.add(f.getName());
            }
        }

        content.removeAllViews();
        LinearLayout contener = new LinearLayout(this);
        for(int i = 0; i < folders.size(); i++){
            FolderButton button = new FolderButton(FileManagerActivity.this, R.drawable.ic_action_file_folder_open_white, folders.get(i), folders.get(i));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FolderButton bt = (FolderButton)v;
                    Intent intent = new Intent(FileManagerActivity.this, GalleryActivity.class);
                    intent.putExtra("path", bt.path);
                    startActivity(intent);
                }
            });
            button.setOnLongClickListener(folderOptions);
            LinearLayout.LayoutParams contenerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300);
            if(i % 2 == 0) {
                if(i != folders.size() - 1){
                    contener.addView(button);
                }
                else{
                    LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(display.getWidth()/2, ViewGroup.LayoutParams.MATCH_PARENT);
                    buttonParams.gravity = Gravity.LEFT;
                    button.setLayoutParams(buttonParams);
                    contener.addView(button);
                    content.addView(contener);
                }
            }
            else {
                contener.addView(button);
                content.addView(contener);
                contener = new LinearLayout(this);
            }
            contener.setLayoutParams(contenerParams);
        }
    }

    private View.OnLongClickListener folderOptions = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            final FolderButton fb = (FolderButton)v;

            String[] options = {"usun"};

            AlertDialog.Builder alert = new AlertDialog.Builder(FileManagerActivity.this);
            alert.setTitle("Folder");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch(which){
                        case 0:

                            deleteGallery(fb.path);
                            createButtonsForDirectories();

                            break;
                    }
                }
            });
            alert.show();

            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    private void deleteGallery(final String path){
        AlertDialog.Builder alert = new AlertDialog.Builder(FileManagerActivity.this);
        alert.setTitle("Usuwanie!");
        alert.setMessage("Na pewno chcesz usunac folder?");
        //delete
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/" + path);

                ArrayList<String> folders = new ArrayList<String>();

                File parent = new File(file.getParent());
                for(File f : parent.listFiles())
                    if(f.isDirectory())
                        folders.add(f.getName());

                String[] stringArray = folders.toArray(new String[folders.size()]);

                for (File f : file.listFiles()) {
                    if (f.isFile())
                        f.delete();
                }

                if(file.exists() && file.isDirectory())
                    file.delete();

                createButtonsForDirectories();

                dialog.cancel();
            }
        });
        //cancel
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alert.show();
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
