package com.example.tomek.project1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CollageEditActivity extends AppCompatActivity {

    private ImageView flipImage;
    private ImageView rotateImage;
    private ImageView done;
    private ImageView imageView;
    private ImageView decline;
    private Bitmap bitmap;
    private Bitmap originalBitmap;
    private LinearLayout scroll;

    private float[][] rotationArray = new float[][]{
            { -1.0f, 1.0f},
            { 1.0f, -1.0f},
            { -1.0f, 1.0f},
            { -1.0f, -1.0f}
    };

    private int currentFlip = 0;

    private float[][] effectsArray = new float[][] {
            {
                    1, 0, 0, 0, 0,
                    0, 1, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_edit);

        //get bmp from path
        String path = (String) getIntent().getSerializableExtra("bmp_path");
        bitmap = originalBitmap = Tools.decodeImage(path, 0);
        Bitmap small = Tools.decodeImage(path, 4);

        imageView = (ImageView) findViewById(R.id.collageEdit_imageView);
        imageView.setImageBitmap(bitmap);

        scroll = (LinearLayout) findViewById(R.id.collageEditScrollView);
        for(int i = 0; i < effectsArray.length; i++) {
            EffectPreview ep;
            if(i == 0) {
                ep = new EffectPreview(CollageEditActivity.this, small, effectsArray[i], true);
            } else {
                ep = new EffectPreview(CollageEditActivity.this, small, effectsArray[i], false);
            }
            ep.setOnClickListener(aaa);
            scroll.addView(ep);
        }

        //flip image
        flipImage = (ImageView)findViewById(R.id.collageEdit_flipImage);
        flipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.flipBitmap(bitmap, rotationArray[currentFlip]);
                originalBitmap = Tools.flipBitmap(originalBitmap, rotationArray[currentFlip]);
                currentFlip++;
                if (currentFlip == rotationArray.length)
                    currentFlip = 0;
                imageView.setImageBitmap(bitmap);
            }
        });

        //rotate
        rotateImage = (ImageView)findViewById(R.id.collageEdit_rotateImage);
        rotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.rotateBitmap(bitmap);
                originalBitmap = Tools.rotateBitmap(originalBitmap);
                imageView.setImageBitmap(bitmap);
            }
        });

        done = (ImageView) findViewById(R.id.collageEdit_done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/tmp_collage");
                FileOutputStream fs = null;
                try {

                    fs = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fs);
                    fs.flush();
                    fs.close();

                    Intent intent = new Intent();
                    intent.putExtra("bmp_path", file.getAbsolutePath());
                    setResult(CollageActivity.RESULT_OK, intent);
                    finish();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        decline = (ImageView) findViewById(R.id.collageEdit_decline);
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(CollageActivity.RESULT_CANCELED, intent);
                finish();
            }
        });

    }

    private View.OnClickListener aaa = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EffectPreview ef = (EffectPreview)v;
            if (ef.isDefault) {
                bitmap = originalBitmap;
            } else {
                bitmap = Tools.changeColor(bitmap, ef.getEffect());
            }
            imageView.setImageBitmap(bitmap);
        }
    };

}
