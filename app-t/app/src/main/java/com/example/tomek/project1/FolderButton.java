package com.example.tomek.project1;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Tomek on 16.09.2015.
 */
public class FolderButton extends LinearLayout{

    public String path;

    private TextView text;
    private ImageView image;

    public FolderButton(Context context, int resourceId, String text, String path){
        super(context);
        //path
        this.path = path;

        //image
        this.image = new ImageView(context);
        this.image.setImageResource(resourceId);
        this.image.setScaleType(ImageButton.ScaleType.FIT_CENTER);
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,200);
        this.image.setLayoutParams(imageParams);

        //text
        this.text = new TextView(context);
        this.text.setText(path);
        this.text.setTextColor(Color.WHITE);
        this.text.setBackgroundColor(Color.BLACK);
        this.text.setTextSize(20);
        this.text.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,100);
        this.text.setLayoutParams(textParams);

        //layout
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        this.setOrientation(VERTICAL);
        params.weight = 1;
        this.setLayoutParams(params);
        this.addView(this.image);
        this.addView(this.text);
    }
}
