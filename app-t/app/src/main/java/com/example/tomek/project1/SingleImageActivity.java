package com.example.tomek.project1;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SingleImageActivity extends AppCompatActivity {

    private String path;
    private ImageView imageView;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");

        imageView = (ImageView)findViewById(R.id.sImageView);

        bitmap = Tools.decodeImage(path, 2);
        imageView.setImageBitmap(bitmap);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SingleImageActivity.this, EditActivity.class);
                intent.putExtra("path", path);
                Log.d("TC", path);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
//            case R.id.action_settings:
//                return true;
            case R.id.action_delete_singleImage:
                deleteItem();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteItem(){
        AlertDialog.Builder alert = new AlertDialog.Builder(SingleImageActivity.this);
        alert.setTitle("Delete!");
        alert.setMessage("Do you really want to delete this image?");
        //delete
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(path);
                Intent intent = new Intent(SingleImageActivity.this, GalleryActivity.class);
                String parentFolderName = new File(file.getParent()).getName();
                intent.putExtra("path", parentFolderName);
                if(file.isFile())
                    file.delete();
                dialog.cancel();
                startActivity(intent);
            }
        });
        //cancel
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alert.show();
    }


}
