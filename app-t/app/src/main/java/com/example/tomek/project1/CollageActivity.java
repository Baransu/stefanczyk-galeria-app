package com.example.tomek.project1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CollageActivity extends Activity {

    private ImageView imageID;

    public static final int CAMERA_PICTURE = 1;
    public static final int EDIT_PICTURE = 2;
    private static final int SELECT_PICTURE = 3;

    private Bitmap collageBMP;

    private String[] folders;

    private FrameLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);

        layout = (FrameLayout)findViewById(R.id.collageLayout);


        ArrayList<HashMap<String, Integer>> lista = (ArrayList<HashMap<String, Integer>>) getIntent().getSerializableExtra("lista");

        for(int i = 0; i < lista.size(); i++) {
            int x = lista.get(i).get("x");
            int y = lista.get(i).get("y");
            int w = lista.get(i).get("width");
            int h = lista.get(i).get("height");

            ImageView image = new ImageView(this);
            image.setImageResource(R.drawable.camera_ic);
            image.setX(x);
            image.setY(y);
            image.setLayoutParams(new FrameLayout.LayoutParams(w, h));
            image.setScaleType(ImageView.ScaleType.CENTER);
            image.setOnClickListener(funkcja);
            layout.addView(image);
        }

        lista.clear();
    }

    private View.OnClickListener funkcja = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            imageID = (ImageView)v;

            String[] options = {"edytuj", "kamera", "galeria", "zapisz kolaz do zdjecia"};

            AlertDialog.Builder alert = new AlertDialog.Builder(CollageActivity.this);
            alert.setTitle("Kolaz");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
//                    Log.d("TC", ""+which);
                    switch(which){
                        case 0:

                            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageID.getDrawable());
                            Bitmap bitmap = bitmapDrawable .getBitmap();

                            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/tmp_collage");
                            FileOutputStream fs = null;
                            try {

                                fs = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fs);
                                fs.flush();
                                fs.close();

                                Intent cokolwiek = new Intent(CollageActivity.this, CollageEditActivity.class);
                                cokolwiek.putExtra("bmp_path", file.getAbsolutePath());
                                startActivityForResult(cokolwiek, EDIT_PICTURE);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            break;
                        case 1:

                            Intent camera = new Intent(CollageActivity.this, CollageCameraActivity.class);
                            startActivityForResult(camera, CAMERA_PICTURE);

                            break;
                        case 2:

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_PICK);
                            startActivityForResult(Intent.createChooser(intent, "Wybierz zdjecie"), SELECT_PICTURE);

                            break;
                        case 3:

                            collageBMP = Tools.bitmapFromView(layout);
                            saveImg();

                            break;
                    }
                }
            });
            alert.show();

        }
    };

    private void saveImg(){
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski");

        int size = file.listFiles().length;
        Log.d("TC", ""+size);
        ArrayList<String> fol = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            if (file.listFiles()[i].isDirectory()) {
                fol.add(file.listFiles()[i].getName());
            }
        }

        folders = new String[fol.size()];
        for(int i = 0; i < fol.size(); i++) {
            folders[i] = fol.get(i);
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(CollageActivity.this);
        alert.setTitle("Folder zapisu: ");
        alert.setItems(folders, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                final int aaa = which;
                AlertDialog.Builder aalert = new AlertDialog.Builder(CollageActivity.this);
                aalert.setTitle("Zapis");
                aalert.setMessage("Podaj nazwe pliku");
                final EditText input = new EditText(CollageActivity.this);
                aalert.setView(input);
                aalert.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TomaszCichocinski/" + folders[aaa] + "/" + input.getText().toString());
                        FileOutputStream fs = null;
                        try {

                            fs = new FileOutputStream(folder);
                            collageBMP.compress(Bitmap.CompressFormat.PNG, 100, fs);
                            fs.flush();
                            fs.close();

                            Toast.makeText(CollageActivity.this, "zapisano w " + folders[aaa], Toast.LENGTH_SHORT).show();


                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                });

                aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                aalert.show();
            }
        });
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                Uri selectedImage = data.getData();

                imageID.setScaleType(ImageView.ScaleType.CENTER_CROP);
                try {
                    imageID.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(requestCode == EDIT_PICTURE) {
                Bundle extras = data.getExtras();
                String path = (String) extras.get("bmp_path");
                Bitmap bmp = Tools.decodeImage(path, 0);
                imageID.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageID.setImageBitmap(bmp);

            }
            if(requestCode == CAMERA_PICTURE) {
                Log.d("TC", "kamera");
                Bundle extras = data.getExtras();
                byte[] d = (byte[]) extras.get("bmp");
                Bitmap bmp = BitmapFactory.decodeByteArray(d, 0, d.length);

                bmp = RotateBitmap(bmp, 90);
                imageID.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageID.setImageBitmap(bmp);
            }
        }
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

}
