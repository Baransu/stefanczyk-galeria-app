package com.example.tomek.project1;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.util.ArrayList;

public class FontActivity extends AppCompatActivity {

    private EditText editText;
    private ImageView colorPicker;
    private ImageView outlinePicker;

    private LinearLayout fontView;

    private RelativeLayout previewLayout;

    private ArrayList<Font> fonts = new ArrayList<Font>();

    private FontPreview previewText;

    private Typeface currentTypeface;

    private String currentText = "";

    private RelativeLayout mainLayout;

    private ColorPicker globalColorPicker;

    private int color = Color.WHITE;
    private int outlineColor = Color.WHITE;

    private String currentTypefaseString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_font);

        mainLayout = (RelativeLayout)findViewById(R.id.fontActivity_relativeLayout);
        globalColorPicker = new ColorPicker(FontActivity.this);
        mainLayout.addView(globalColorPicker);
        globalColorPicker.setVisibility(View.INVISIBLE);

        editText = (EditText)findViewById(R.id.fontInputBox);
        colorPicker = (ImageView)findViewById(R.id.fontColorPicker);
        outlinePicker = (ImageView)findViewById(R.id.outlinePicker);
        fontView = (LinearLayout)findViewById(R.id.fontSelectView);

        previewLayout = (RelativeLayout)findViewById(R.id.fontPreview);


        AssetManager assetManager = getAssets();
        try {
            String[] lista = assetManager.list("fonts"); // fonts to nazwa podfolderu w assets

            for(String s: lista) {
                Font f = new Font(FontActivity.this, "text", s, Typeface.createFromAsset(getAssets(), "fonts/" + s));
                fonts.add(f);
                f.setOnClickListener(funkcja);
                fontView.addView(f);
                currentTypeface = Typeface.createFromAsset(getAssets(), "fonts/" + s);
                currentTypefaseString = s;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                currentText = s.toString();
                changeText();
            }
        };

        editText.addTextChangedListener(textWatcher );

        ImageView accept = (ImageView)findViewById(R.id.fontAccept);
        ImageView decline = (ImageView)findViewById(R.id.fontDecline);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("text", currentText);
                intent.putExtra("font", currentTypefaseString);
                intent.putExtra("color", color);
                intent.putExtra("outline", outlineColor);
                setResult(777, intent);
                finish();
            }
        });

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(666, intent);
                finish();
            }
        });



        outlinePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalColorPicker.isOutline = true;
                globalColorPicker.setVisibility(View.VISIBLE);
            }
        });

        colorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalColorPicker.isOutline = false;
                globalColorPicker.setVisibility(View.VISIBLE);
            }
        });

    }

    //color
    public void changeColor(int kolor){

        color = kolor;
        changeText();

    }

    //outline
    public void changeOutline(int kolor){

        outlineColor = kolor;
        changeText();
    }

    private void changeText() {
        previewLayout.removeAllViews();
        previewText = new FontPreview(FontActivity.this, currentText, currentTypeface, color, outlineColor);
        previewLayout.addView (previewText);
    }

    private View.OnClickListener funkcja = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Font f = (Font)v;
            currentTypeface = f.getFont();
            currentTypefaseString = f.font;
            changeText();
        }
    };
}
