package com.example.tomek.project1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    String rootFolder = "/TomaszCichocinski/";
    String[] foldersName = {"Obrazy", "Osoby", "Przyroda", "Zdjecia"};

    private Folder folder;
    public static ArrayList<String> imagesList = new ArrayList<String>();
    private static ArrayList<Bitmap> imageBitmaps = new ArrayList<Bitmap>();
    private static ArrayList<SliderFragment> imagesFragments = new ArrayList<SliderFragment>();

    public static Socket socket;
    {
        try {
            socket = IO.socket(Consts.IP_ADRESS);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static int currentPosition;
    private static SliderAdapter sliderAdapter;
    private static Bitmap bbb;

    private static Context con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        con = MainActivity.this;
        setContentView(R.layout.activity_main);

        createDirectoriesIfNotExists();

        LinearLayout button1 = (LinearLayout)findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout button2 = (LinearLayout) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FileManagerActivity.class);
                intent.putExtra("folders", foldersName);
                startActivity(intent);
            }
        });

        LinearLayout button3 = (LinearLayout) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SelectCollageActivity.class));
            }
        });

        LinearLayout button4 = (LinearLayout) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternet(MainActivity.this)) {
                    startActivity(new Intent(MainActivity.this, SerwerImagesActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, "Brak internetu", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if(isInternet(MainActivity.this)) {

            bbb = BitmapFactory.decodeResource(MainActivity.this.getResources(), R.drawable.placeholder);
            imagesFragments.add(new SliderFragment());
            imageBitmaps.add(bbb);

            sliderAdapter =  new SliderAdapter(getSupportFragmentManager(), imagesFragments, imageBitmaps);
            sliderAdapter.notifyDataSetChanged();
            ViewPager viewPager = (ViewPager) findViewById(R.id.mainSlider);
            viewPager.setAdapter(sliderAdapter);

            socket.connect();

            updateList(MainActivity.this);

            socket.on("image-success", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            byte[] img = (byte[]) args[0];

                            Bundle argss = new Bundle();
                            argss.putSerializable("img", img);

                        }
                    });
                }
            });

            socket.on("image-info-failed", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Nie mozna pobrac infomracji", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        } else {
            imagesFragments.add(new SliderFragment());
            imageBitmaps.add(BitmapFactory.decodeResource(MainActivity.this.getResources(), R.drawable.sam_didier1));
            Toast.makeText(MainActivity.this, "brak internetu", Toast.LENGTH_SHORT).show();
        }

    }

    public static void updateList(Context context){
        if(isInternet(context)) {
            JSONObject data = new JSONObject();
            socket.emit("image-info-request", data);
            socket.on("image-info-success", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    ClientPlayer.runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject data = (JSONObject) args[0];
                            JSONArray imgs;
                            try {
                                imgs = data.getJSONArray("images");
                                imagesList = new ArrayList<String>();
                                imagesList.add("lol");
                                for(int i = 0; i < imgs.length(); i++) {
                                    imagesList.add(imgs.get(i).toString());
                                    imagesFragments.add(new SliderFragment());
                                    imageBitmaps.add(bbb);
                                }

                                sliderAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

    public static void uImg(int position) {
        try {
            updateImg(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateImg(int position ) throws JSONException {
        if(imagesList.size() > 0 && position < imagesList.size()) {
            Log.d("TC", "respond");
            JSONObject data = new JSONObject();
            currentPosition = position;
            data.put("img", imagesList.get(position));

            socket.emit("image-request", data);
            socket.on("image-respond", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    ClientPlayer.runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            byte[] img = (byte[]) args[0];

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0, img.length, options);
                            Log.d("TC", "main: "+img);
                            imageBitmaps.set(currentPosition, bitmap);
                            sliderAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
    }

    public static boolean isInternet(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }
        else{
            return true;
        }
    }

    private void createDirectoriesIfNotExists() {
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File[] files = file.listFiles();
        File root = new File(file.getPath() + rootFolder);
        if(!root.exists())
            root.mkdir();
        for(File f : files){
            if(f.isDirectory()){
                for(String s : foldersName){
                    File tempFile = new File(file.getPath() + rootFolder + s);
                    if(!tempFile.exists()) {
                        tempFile.mkdir();
                    }
                }
            }
        }
    }

    private void deleteFolders(){
        Intent intent = new Intent(MainActivity.this, FileManagerActivity.class);
        intent.putExtra("clear", true);
        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        for(String s : foldersName){
            File file = new File(root.getPath() + rootFolder + s);
            if(file.exists())
                file.delete();
        }
        File nameFolder = new File(root.getPath() + rootFolder);
        if(nameFolder.exists())
            nameFolder.delete();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

class ClientPlayer extends Activity {

    public static Handler UIHandler;

    static
    {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

}