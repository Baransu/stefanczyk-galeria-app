package com.example.sabi;

import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.pawel.aplikacja.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout frameLayout;

    private String rootFolder = "/SabinaBiga/";

    private ImageView cameraOptionsButton1;
    private ImageView cameraOptionsButton2;
    private ImageView cameraOptionsButton3;

    private Camera.Parameters camParams;

    int cameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
    private CameraPreview cameraPreview;

    private Bitmap bitmap;
    private boolean waitForAccept = false;

    private SeekBar cameraZoom;

    private String[] folders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        initCamera(cameraOrientation);

        cameraPreview = new CameraPreview(CameraActivity.this, camera);
        frameLayout = (FrameLayout) findViewById(R.id.cameraLayoutMain);
        frameLayout.addView(cameraPreview);
        camParams = camera.getParameters();

        ImageView takePictureButton = (ImageView) findViewById(R.id.takePictureButton);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(null, null, camPictureCallback);
            }
        });

        cameraOptionsButton1 = (ImageView) findViewById(R.id.cameraOptionsButton1);
        cameraOptionsButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraSizeOptions();
            }
        });

        cameraOptionsButton2 = (ImageView) findViewById(R.id.cameraOptionsButton2);
        cameraOptionsButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraBalanceOptions();
            }
        });

        cameraOptionsButton3 = (ImageView) findViewById(R.id.cameraOptionsButton3);
        cameraOptionsButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraExposureOptions();
            }
        });

        cameraZoom = (SeekBar)findViewById(R.id.camera_zoom);
        cameraZoom.setMax(camParams.getMaxZoom());
        cameraZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                camParams.setZoom(val);
                camera.setParameters(camParams);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }

        });

        ImageView accept = (ImageView) findViewById(R.id.acceptButton);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(waitForAccept) {

                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/SabinaBiga");

                    int size = file.listFiles().length;
                    ArrayList<String> fol = new ArrayList<String>();
                    for (int i = 0; i < size; i++) {
                        if (file.listFiles()[i].isDirectory()) {
                            fol.add(file.listFiles()[i].getName());
                        }
                    }

                    folders = new String[fol.size()];
                    for(int i = 0; i < 1; i++) {
                        folders[i] = fol.get(i);
                    }

                    AlertDialog.Builder aalert = new AlertDialog.Builder(CameraActivity.this);
                    aalert.setTitle("Folder zapisu: ");
                    aalert.setItems(folders, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                            String d = dFormat.format(new Date());

                            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/SabinaBiga/" + folders[which] + "/" + d);
                            FileOutputStream fs = null;
                            try {

                                fs = new FileOutputStream(folder);
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fs);
                                fs.flush();
                                fs.close();

                                Toast.makeText(CameraActivity.this, "Zdjecie zapisane w folderze " + folders[which], Toast.LENGTH_SHORT).show();

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    aalert.show();


                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Tapeta");
                    alert.setMessage("Czy na chcesz ustwic tapete?");
                    //delete
                    alert.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            WallpaperManager wpm = (WallpaperManager)getSystemService(WALLPAPER_SERVICE);
                            try {
                                wpm.setBitmap(bitmap);
                                Toast.makeText(CameraActivity.this, "Tapeta zmieniona", Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            camera.startPreview();
                            waitForAccept = false;
                            dialog.cancel();
                        }
                    });
                    //cancel
                    alert.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            camera.startPreview();
                            waitForAccept = false;
                            dialog.cancel();
                        }
                    });

                    alert.show();



                }
            }
        });

        ImageView decline = (ImageView) findViewById(R.id.declineButton);
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(waitForAccept) {
                    camera.startPreview();
                    waitForAccept = false;
                }
            }
        });

    }

    private void cameraSizeOptions() {
        if (camParams != null) {
            final List<Camera.Size> resolutions = camParams.getSupportedPictureSizes();
            String[] options = new String[resolutions.size()];
            for (int i = 0; i < resolutions.size(); i++) {
                options[i] = resolutions.get(i).width + "x" + resolutions.get(i).height;
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Rozdzielczosc");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setPictureSize(resolutions.get(which).width, resolutions.get(which).height);
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private void cameraBalanceOptions() {
        if (camParams != null) {
            final List<String> balance = camParams.getSupportedWhiteBalance();
            String[] options = new String[balance.size()];
            for (int i = 0; i < balance.size(); i++) {
                options[i] = balance.get(i);
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Balans bieli");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setWhiteBalance(balance.get(which));
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private void cameraExposureOptions() {
        if (camParams != null) {
            int min = camParams.getMinExposureCompensation();
            int max = camParams.getMaxExposureCompensation();
            final int[] options = new int[max + Math.abs(min)];
            String[] opt = new String[max + Math.abs(min)];
            int cur = 0;
            for (int i = min; i < max; i++) {
                options[cur] = i;
                opt[cur++] = "" + i;
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
            alert.setTitle("Ekspozycja");
            alert.setItems(opt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setExposureCompensation(options[which]);
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            if(!waitForAccept) {

                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                Bitmap bmp = b;
                switch (getResources().getConfiguration().orientation) {
                    //does not work (rotation locked)
                    case Configuration.ORIENTATION_LANDSCAPE:
                        bmp = RotateBitmap(b, 90);
                        break;
                    //it work
                    case Configuration.ORIENTATION_PORTRAIT:
                        bmp = RotateBitmap(b, 90);
                        break;
                }

                bitmap = bmp;
                waitForAccept = true;
            }
        }
    };

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void initCamera(int orientation) {
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            Intent intent = new Intent(CameraActivity.this, MainActivity.class);
            Toast.makeText(CameraActivity.this, "Kamera nie dostepna", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        } else {
            cameraId = getCameraId(orientation);
            if (cameraId < 0) {
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
            } else {
                camera = Camera.open();
            }
        }
    }

    private int getCameraId(int orientation) {
        int camerasCount = Camera.getNumberOfCameras();
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            // 0 - back camera
            // 1 - front camera

            if (orientation == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return 0;
            } else if (orientation == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return 1;
            }
        }
        return -1;
    }
}
