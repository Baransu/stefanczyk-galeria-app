package com.example.sabi;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Circle extends View {

    private int mRadius = 200;
    private int mCircleStrokeWidth = 3;
    private int mCircleX = 0;
    private int mCircleY = 0;

    public Circle(Context context, int radius) {
        super(context);
        this.mRadius = radius;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mCircleStrokeWidth);
        paint.setColor(Color.WHITE);
        mCircleX = getWidth() / 2;
        mCircleY = getHeight() / 2 - mRadius / 2;
        canvas.drawCircle(mCircleX, mCircleY, mRadius, paint);
    }
}
