package com.example.sabi;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;

public class FontPreview extends View {

    private String text;
    private Typeface tf;
    public float holdModifierX = 0;
    public float holdModifierY = 0;
    private int fontColor;
    private int outlineColor;
    private Paint paint;

    public FontPreview(Context context, String text, Typeface tf, int fontColor, int outlineColor) {
        super(context);

        this.text = text;
        this.tf = tf;
        this.fontColor = fontColor;
        this.outlineColor = outlineColor;

        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.reset();            // czyszczenie
        this.paint.setAntiAlias(true);    // wygładzanie
        this.paint.setTextSize(60);        // wielkośc fonta
        this.paint.setTypeface(this.tf); // czcionka

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(Math.round(this.paint.measureText(text)) + 1, 80);
        setLayoutParams(p);

//        setBackgroundColor(Color.MAGENTA);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(fontColor);
        canvas.drawText(text, 0, 60, paint);

        //teraz krawędź, po wypełnieniu aby była ponad nim, rysuje ten sam napis
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(outlineColor);
        canvas.drawText(text, 0, 60, paint);
    }
}
