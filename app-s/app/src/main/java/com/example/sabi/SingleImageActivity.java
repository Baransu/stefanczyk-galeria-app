package com.example.sabi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.pawel.aplikacja.R;

import java.io.File;

public class SingleImageActivity extends AppCompatActivity {

    private String path;
    private ImageView imageView;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");

        imageView = (ImageView) findViewById(R.id.sImageView);

        bitmap = Tools.decodeImage(path, 2);
        imageView.setImageBitmap(bitmap);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SingleImageActivity.this, EditActivity.class);
                intent.putExtra("path", path);
                Log.d("TC", path);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
//            case R.id.action_settings:
//                return true;
            case R.id.action_delete_singleImage:
                deleteItem();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteItem() {
        AlertDialog.Builder alert = new AlertDialog.Builder(SingleImageActivity.this);
        alert.setTitle("Usuwanie!");
        alert.setMessage("Chcesz usunac to zdjecie?");
        //delete
        alert.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(path);
                Intent intent = new Intent(SingleImageActivity.this, GalleryActivity.class);
                String parentFolderName = new File(file.getParent()).getName();
                intent.putExtra("path", parentFolderName);
                if (file.isFile())
                    file.delete();
                dialog.cancel();
                startActivity(intent);
            }
        });
        //cancel
        alert.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alert.show();
    }


}
