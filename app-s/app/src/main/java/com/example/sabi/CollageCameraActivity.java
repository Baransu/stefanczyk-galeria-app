package com.example.sabi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pawel.aplikacja.R;

import java.util.List;

public class CollageCameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout frameLayout;

    private Camera.Parameters camParams;

    int cameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
    private CameraPreview cameraPreview;

    private byte[] bmpData;

    private ImageView accept;
    private ImageView decline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_camera);

        initCamera(cameraOrientation);

        cameraPreview = new CameraPreview(CollageCameraActivity.this, camera);
        frameLayout = (FrameLayout) findViewById(R.id.cameraLayoutMain);
        frameLayout.addView(cameraPreview);
        camParams = camera.getParameters();

        accept = (ImageView) findViewById(R.id.acceptPictureButton);
        accept.setVisibility(View.INVISIBLE);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("bmp", bmpData);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        decline = (ImageView) findViewById(R.id.declinePictureButton);

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });


        ImageView takePictureButton = (ImageView) findViewById(R.id.takePictureButton);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(null, null, camPictureCallback);
            }
        });

        if (camParams != null) {
            cameraSizeOptions();
        }
    }

    private void cameraSizeOptions() {
        if (camParams != null) {
            final List<Camera.Size> resolutions = camParams.getSupportedPictureSizes();
            String[] options = new String[resolutions.size()];
            for (int i = 0; i < resolutions.size(); i++) {
                options[i] = resolutions.get(i).width + "x" + resolutions.get(i).height;
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(CollageCameraActivity.this);
            alert.setTitle("Rozdzielczosc");
            alert.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    camParams.setPictureSize(resolutions.get(which).width, resolutions.get(which).height);
                    camera.setParameters(camParams);
                }
            });
            alert.show();
        }
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            bmpData = data;

            decline.setVisibility(View.VISIBLE);
            accept.setVisibility(View.VISIBLE);

        }
    };

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void initCamera(int orientation) {
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            Intent intent = new Intent(CollageCameraActivity.this, MainActivity.class);
            Toast.makeText(CollageCameraActivity.this, "Kamera niedostepna", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        } else {
            cameraId = getCameraId(orientation);
            if (cameraId < 0) {
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
            } else {
                camera = Camera.open();
            }
        }
    }

    private int getCameraId(int orientation) {
        int camerasCount = Camera.getNumberOfCameras();
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            // 0 - back camera
            // 1 - front camera

            if (orientation == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return 0;
            } else if (orientation == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return 1;
            }
        }
        return -1;
    }

}
