package com.example.sabi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.pawel.aplikacja.R;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    String rootFolder = "/SabinaBiga/";
    String[] foldersName = {"Miejsca", "Osoby", "Rzeczy"};

    private Folder folder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + rootFolder);
        if (f.exists() && f.listFiles().length > 0) {
            foldersName = new String[f.listFiles().length];
            for (int i = 0; i < f.listFiles().length; i++) {
                foldersName[i] = f.listFiles()[i].getName();
            }
        }

        createDirectoriesIfNotExists();

        LinearLayout button1 = (LinearLayout) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout button2 = (LinearLayout) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FileManagerActivity.class);
                intent.putExtra("folders", foldersName);
                startActivity(intent);
            }
        });

        LinearLayout button3 = (LinearLayout) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SelectCollageActivity.class));
            }
        });
    }

    private void createDirectoriesIfNotExists() {
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File[] files = file.listFiles();
        File root = new File(file.getPath() + rootFolder);
        if (!root.exists())
            root.mkdir();
        for (File f : files) {
            if (f.isDirectory()) {
                for (String s : foldersName) {
                    File tempFile = new File(file.getPath() + rootFolder + s);
                    if (!tempFile.exists()) {
                        tempFile.mkdir();
                    }
                }
            }
        }
    }

//    private void deleteFolders() {
//        Intent intent = new Intent(MainActivity.this, FileManagerActivity.class);
//        intent.putExtra("clear", true);
//        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//        for (String s : foldersName) {
//            File file = new File(root.getPath() + rootFolder + s);
//            if (file.exists())
//                file.delete();
//        }
//        File nameFolder = new File(root.getPath() + rootFolder);
//        if (nameFolder.exists())
//            nameFolder.delete();
//    }
}
