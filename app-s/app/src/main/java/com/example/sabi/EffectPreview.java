package com.example.sabi;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class EffectPreview extends ImageView {

    private Bitmap bmp;
    private float[] effect;
    public boolean isDefault = false;


    public float[] getEffect() {
        return effect;
    }

    public EffectPreview(Context context, Bitmap bitmap, float[] effect, boolean isDefault) {
        super(context);
        this.isDefault = isDefault;
        this.bmp = Tools.changeColor(bitmap, effect);
        this.effect = effect;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);
        setImageBitmap(this.bmp);
        setScaleType(ImageView.ScaleType.CENTER);
    }

}
