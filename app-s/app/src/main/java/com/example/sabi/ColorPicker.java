package com.example.sabi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.pawel.aplikacja.R;

public class ColorPicker extends RelativeLayout {

    private ImageView accept;
    private ImageView decline;
    private ImageView img;
    private LinearLayout buttons;

    private final Context context;

    public boolean isOutline = false;

    private int kolor = 0;

    private Bitmap bmp;

    public ColorPicker(final Context context) {
        super(context);

        this.context = context;
        setBackgroundColor(Color.argb(166, 0, 0, 0));
        buttons = new LinearLayout(context);
        accept = new ImageView(context);
        decline = new ImageView(context);
        img = new ImageView(context);

        RelativeLayout.LayoutParams a = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(a);

        buttons.setOrientation(LinearLayout.HORIZONTAL);
        buttons.setBackgroundColor(Color.rgb(200, 25, 25));

        LinearLayout.LayoutParams butParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
        buttons.setLayoutParams(butParams);

        LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500);
        img.setLayoutParams(imgParams);
        img.setY(100);

        bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.wheel);
        img.setImageBitmap(bmp);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT / 2, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = 1;

        accept.setLayoutParams(params);
        decline.setLayoutParams(params);

        accept.setImageResource(R.drawable.acc);
        decline.setImageResource(R.drawable.del);

        decline.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.INVISIBLE);
            }
        });

        accept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isOutline) {
                    ((FontActivity) context).changeOutline(kolor);
                } else {
                    ((FontActivity) context).changeColor(kolor);
                }

                setVisibility(View.INVISIBLE);
            }
        });

        buttons.addView(decline);
        buttons.addView(accept);

        img.setDrawingCacheEnabled(true);

        img.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ImageView image = (ImageView) v;
                bmp = image.getDrawingCache();

                switch (event.getAction()) {

                    case MotionEvent.ACTION_MOVE:

                        if (event.getX() < bmp.getWidth() && event.getY() < bmp.getHeight()) {
                            kolor = bmp.getPixel((int) event.getX(), (int) event.getY());
                            int R = (kolor >> 16) & 0xff;
                            int G = (kolor >> 8) & 0xff;
                            int B = kolor & 0xff;

                            buttons.setBackgroundColor(Color.rgb(R, G, B));
                        }

                        break;

                    case MotionEvent.ACTION_DOWN:

                        if (event.getX() < bmp.getWidth() && event.getY() < bmp.getHeight()) {
                            kolor = bmp.getPixel((int) event.getX(), (int) event.getY());
                            int R = (kolor >> 16) & 0xff;
                            int G = (kolor >> 8) & 0xff;
                            int B = kolor & 0xff;

                            buttons.setBackgroundColor(Color.rgb(R, G, B));
                        }


                        break;
                    case MotionEvent.ACTION_UP:

                        if (event.getX() < bmp.getWidth() && event.getY() < bmp.getHeight()) {
                            kolor = bmp.getPixel((int) event.getX(), (int) event.getY());
                            int R = (kolor >> 16) & 0xff;
                            int G = (kolor >> 8) & 0xff;
                            int B = kolor & 0xff;

                            buttons.setBackgroundColor(Color.rgb(R, G, B));
                        }

                        break;
                }

                return true;
            }
        });

        addView(img);
        addView(buttons);
    }

}
