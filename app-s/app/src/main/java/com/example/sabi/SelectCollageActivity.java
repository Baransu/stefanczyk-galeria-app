package com.example.sabi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.pawel.aplikacja.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectCollageActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_collage);

        ImageView img1 = (ImageView) findViewById(R.id.imageSelect1);
        ImageView img2 = (ImageView) findViewById(R.id.imageSelect2);
        ImageView img3 = (ImageView) findViewById(R.id.imageSelect3);
        ImageView img4 = (ImageView) findViewById(R.id.imageSelect4);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectCollageActivity.this, CollageActivity.class);
                intent.putExtra("lista", getList(0));
                startActivity(intent);
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectCollageActivity.this, CollageActivity.class);
                intent.putExtra("lista", getList(1));
                startActivity(intent);
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectCollageActivity.this, CollageActivity.class);
                intent.putExtra("lista", getList(2));
                startActivity(intent);
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectCollageActivity.this, CollageActivity.class);
                intent.putExtra("lista", getList(3));
                startActivity(intent);
            }
        });

    }

    private ArrayList<HashMap<String, Integer>> getList(int id) {

        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();

        ArrayList<HashMap<String, Integer>> lista = new ArrayList<HashMap<String, Integer>>();
        HashMap<String, Integer> mapa;

        switch (id) {
            case 0:

                //img1
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", 0);
                mapa.put("width", width / 2);
                mapa.put("height", 2 * height / 3);
                lista.add(mapa);

                //img2
                mapa = new HashMap<String, Integer>();
                mapa.put("x", width / 2);
                mapa.put("y", 0);
                mapa.put("width", width / 2);
                mapa.put("height", 2 * height / 3);
                lista.add(mapa);

                //img3
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", 2 * height / 3);
                mapa.put("width", width);
                mapa.put("height", height / 3);
                lista.add(mapa);

                break;
            case 1:

                //img1
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", 0);
                mapa.put("width", width / 2);
                mapa.put("height", height);
                lista.add(mapa);

                //img2
                mapa = new HashMap<String, Integer>();
                mapa.put("x", width / 2);
                mapa.put("y", 0);
                mapa.put("width", width / 2);
                mapa.put("height", height / 3);
                lista.add(mapa);

                //img3
                mapa = new HashMap<String, Integer>();
                mapa.put("x", width / 2);
                mapa.put("y", height / 3);
                mapa.put("width", width / 2);
                mapa.put("height", height / 3);
                lista.add(mapa);

                //img4
                mapa = new HashMap<String, Integer>();
                mapa.put("x", width / 2);
                mapa.put("y", 2 * height / 3);
                mapa.put("width", width / 2);
                mapa.put("height", height / 3);
                lista.add(mapa);

                break;
            case 2:

                //img1
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", 0);
                mapa.put("width", width / 3);
                mapa.put("height", height / 4);
                lista.add(mapa);

                //img2
                mapa = new HashMap<String, Integer>();
                mapa.put("x", width / 3);
                mapa.put("y", 0);
                mapa.put("width", width / 3);
                mapa.put("height", height / 4);
                lista.add(mapa);

                //img3
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 2 * width / 3);
                mapa.put("y", 0);
                mapa.put("width", width / 3);
                mapa.put("height", height / 4);
                lista.add(mapa);

                //img4
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", height / 4);
                mapa.put("width", width);
                mapa.put("height", 3 * height / 4);
                lista.add(mapa);

                break;
            case 3:

                //img1
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", 0);
                mapa.put("width", width);
                mapa.put("height", height / 2);
                lista.add(mapa);

                //img2
                mapa = new HashMap<String, Integer>();
                mapa.put("x", 0);
                mapa.put("y", height / 2);
                mapa.put("width", width);
                mapa.put("height", height / 2);
                lista.add(mapa);


                break;
        }

        return lista;
    }


}
