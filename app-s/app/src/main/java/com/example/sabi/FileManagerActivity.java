package com.example.sabi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.pawel.aplikacja.R;

import java.io.File;
import java.util.ArrayList;


public class FileManagerActivity extends AppCompatActivity {

    private LinearLayout content;

    private ArrayList<String> folders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/SabinaBiga/");

        for(int i = 0; i < file.listFiles().length; i++) {
            if(file.listFiles()[i].isDirectory()) {
                folders.add(file.listFiles()[i].getName());
            }
        }

        content = (LinearLayout) findViewById(R.id.content);

        createButtonsForDirectories(folders);

        LinearLayout newFolder = (LinearLayout) findViewById(R.id.newFolder);
        newFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder aalert = new AlertDialog.Builder(FileManagerActivity.this);
                aalert.setTitle("Nowy folder");
                aalert.setMessage("Podaj nazwe nowego folderu");
                final EditText input = new EditText(FileManagerActivity.this);
                aalert.setView(input);
                aalert.setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/SabinaBiga/" + input.getText().toString());
                        if (!folder.exists()) {

                            folders.add(folder.getName());

                            folder.mkdir();
                            createButtonsForDirectories(folders);
                        }
                    }

                });

                aalert.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                aalert.show();
            }
        });

    }

    public void createButtonsForDirectories(final ArrayList<String> folders) {
        content.removeAllViews();
        for (int i = 0; i < folders.size(); i++) {
            FolderButton button = new FolderButton(FileManagerActivity.this, R.drawable.folder, folders.get(i), folders.get(i));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FolderButton bt = (FolderButton) v;
                    Intent intent = new Intent(FileManagerActivity.this, GalleryActivity.class);
                    intent.putExtra("path", bt.path);
                    startActivity(intent);
                }
            });

            content.addView(button);
        }
    }
}
