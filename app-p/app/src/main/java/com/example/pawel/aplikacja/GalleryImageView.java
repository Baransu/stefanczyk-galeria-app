package com.example.pawel.aplikacja;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class GalleryImageView extends ImageView {

    public String path;

    public GalleryImageView(Context context, Bitmap bmp, int weight, String path) {
        super(context);
        this.setImageBitmap(bmp);
        this.path = path;
        this.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = weight;
        this.setLayoutParams(params);
    }
}
