package com.example.pawel.aplikacja;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class GalleryActivity extends AppCompatActivity {

    String rootFolder = "/PawelPaduch/";
    private String path;

    private LinearLayout galleryContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");

        galleryContent = (LinearLayout) findViewById(R.id.galleryContent);

        showGalleryFromDirectory();
    }

    void showGalleryFromDirectory() {
        Random generator = new Random();
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + rootFolder + path);
        File[] fileList = file.listFiles();
        LinearLayout contener = new LinearLayout(this);
        int index = 1;
        int maxIndex = 3;
        Display display = getWindowManager().getDefaultDisplay();
        for (int i = 0; i < fileList.length; i++) {
            //image
            int weight = generator.nextInt(2) + 1;
            Bitmap bmp = Tools.decodeImage(fileList[i].getPath(), 4);
            GalleryImageView imageView = new GalleryImageView(GalleryActivity.this, bmp, weight, fileList[i].getPath());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GalleryImageView img = (GalleryImageView) v;
                    Intent intent = new Intent(GalleryActivity.this, SingleImageActivity.class);
                    intent.putExtra("path", img.path);
                    startActivity(intent);
                }
            });
            //content
            LinearLayout.LayoutParams contenerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, generator.nextInt(150) + 200);
            contener.setLayoutParams(contenerParams);
            if (index == maxIndex) {
                contener.addView(imageView);
                galleryContent.addView(contener);
                contener = new LinearLayout(GalleryActivity.this);
                maxIndex = generator.nextInt(3) + 1;
                index = 0;
            } else if (i == fileList.length - 1) {
                contener.addView(imageView);
                galleryContent.addView(contener);
            } else {
                contener.addView(imageView);
            }
            index++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
//            case R.id.action_settings:
//                return true;
            case R.id.action_delete_gallery:
                deleteGallery();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteGallery() {
        AlertDialog.Builder alert = new AlertDialog.Builder(GalleryActivity.this);
        alert.setTitle("Usuwanie!");
        alert.setMessage("Czy na pewno chcesz usunac?");
        //delete
        alert.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + rootFolder + path);
                Intent intent = new Intent(GalleryActivity.this, FileManagerActivity.class);

                ArrayList<String> folders = new ArrayList<String>();

                File parent = new File(file.getParent());
                for (File f : parent.listFiles())
                    if (f.isDirectory())
                        folders.add(f.getName());

                String[] stringArray = folders.toArray(new String[folders.size()]);
                intent.putExtra("folders", stringArray);

                for (File f : file.listFiles()) {
                    if (f.isFile())
                        f.delete();
                }

                dialog.cancel();
                startActivity(intent);
            }
        });
        //cancel
        alert.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alert.show();
    }
}
