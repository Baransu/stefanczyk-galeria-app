package com.example.pawel.aplikacja;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;

import java.io.File;

public class Tools {

    public static Bitmap decodeImage(String filePath, int compression) {
        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inDensity = 0;
        options.inSampleSize = compression;
        File imgFile = new File(filePath);
        myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        return myBitmap;
    }


    public static Bitmap rotateBitmap(Bitmap bmp) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

        return rotated;
    }

    public static Bitmap flipBitmap(Bitmap bmp, float[] direction) {
        Matrix matrix = new Matrix();

        matrix.postScale(direction[0], direction[1]);

        Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

        return rotated;
    }

    public static Bitmap changeColor(Bitmap bmp, float[] color) {
        ColorMatrix cMatrix = new ColorMatrix();

        cMatrix.set(color);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeContrast(Bitmap bmp, float contrast) {
        ColorMatrix cMatrix = new ColorMatrix();

        float[] changeMatrix = {
                contrast, 0, 0, 0, 0,
                0, contrast, 0, 0, 0,
                0, 0, contrast, 0, 0,
                0, 0, 0, 1, 0
        };

        cMatrix.set(changeMatrix);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeSaturation(Bitmap bmp, float saturation) {
        ColorMatrix cMatrix = new ColorMatrix();

        cMatrix.setSaturation(saturation);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }

    public static Bitmap changeBrightness(Bitmap bmp, float brightness) {
        ColorMatrix cMatrix = new ColorMatrix();

        float[] changeMatrix = {
                1, 0, 0, 0, brightness,
                0, 1, 0, 0, brightness,
                0, 0, 1, 0, brightness,
                0, 0, 0, 1, 0
        };

        cMatrix.set(changeMatrix);

        Paint paint = new Paint();

        Bitmap b = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Canvas canvas = new Canvas(b);
        canvas.drawBitmap(bmp, 0, 0, paint);

        return b;
    }
}
