package com.example.pawel.aplikacja;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Rect extends ImageView {

    private int mRectSize = 125;

    public Bitmap getBmp() {
        return bmp;
    }

    private Bitmap bmp;

    public Rect(final Context context, final Bitmap bmp, Bitmap fullSize) {
        super(context);
        this.setLayoutParams(new ViewGroup.LayoutParams(125, 125));
        this.bmp = fullSize;
        this.setImageBitmap(bmp);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawR(canvas, 0, 0, mRectSize, mRectSize);
    }

    private void drawR(Canvas canvas, int x, int y, int width, int height) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.TRANSPARENT);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(x, y, x + width, y + height, paint);
        paint.setColor(Color.BLACK);
    }
}
