package com.example.pawel.aplikacja;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditActivity extends AppCompatActivity {

    private String path;
    private ImageView imageView;
    private ImageView.ScaleType[] scaleTypes = {ImageView.ScaleType.FIT_CENTER, ImageView.ScaleType.CENTER, ImageView.ScaleType.CENTER_CROP};

    private RelativeLayout mainLayout;
    private int clickCount = 0;

    private ImageView fontEditor;
    private ImageView flipImage;
    private ImageView rotateImage;
    private ImageView colorFilter;
    private ImageView seekControl;
    private ImageView upload;
    private ImageView save;

    private Bitmap bitmap;
    private Bitmap originalBitmap;

    private float[][] rotationArray = new float[][]{
            {-1.0f, 1.0f},
            {1.0f, -1.0f},
            {-1.0f, 1.0f},
            {-1.0f, -1.0f}
    };

    private int currentFlip = 0;

    private float[][] effectsArray = new float[][]{
            {
                    1, 0, 0, 0, 0,
                    0, 1, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    2, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 1, 0
            },
            {
                    -1, 0, 0, 1, 0,
                    0, -1, 0, 1, 0,
                    0, 0, -1, 1, 0,
                    0, 0, 0, 1, 0

            }
    };

    private String[] effectNames = {
            "normal",
            "red",
            "negatyw"
    };

    private LinearLayout seekLayout;
    private SeekBar contrastBar;
    private SeekBar brightnessBar;
    private SeekBar saturationBar;

    private String[] folders;
    private String[] folderPaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");

        imageView = (ImageView) findViewById(R.id.singleImageView);

        mainLayout = (RelativeLayout) findViewById(R.id.singleImageView_mainLayout);

        originalBitmap = bitmap = Tools.decodeImage(path, 2);
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(scaleTypes[clickCount]);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/PawelPaduch/");

        int size = file.listFiles().length;
        folders = new String[size];
        for (int i = 0; i < file.listFiles().length; i++) {
            if (file.listFiles()[i].isDirectory()) {
                folders[i] = file.listFiles()[i].getName();
            }
        }

        save = (ImageView) findViewById(R.id.singleImage_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                alert.setTitle("Folder do zapisu: ");
                alert.setItems(folders, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String d = dFormat.format(new Date());

                        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/PawelPaduch/" + folders[which] + "/" + d);
                        FileOutputStream fss = null;
                        try {

                            fss = new FileOutputStream(folder);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fss);
                            fss.flush();
                            fss.close();

                            Toast.makeText(EditActivity.this, "Zapisano w folderze " + folders[which], Toast.LENGTH_SHORT).show();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
                alert.show();
            }
        });

        //font editor
        fontEditor = (ImageView) findViewById(R.id.singleImage_fontEditor);
        fontEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContactIntent = new Intent(EditActivity.this, FontActivity.class);
                startActivityForResult(pickContactIntent, 777);
            }
        });

        //flip image
        flipImage = (ImageView) findViewById(R.id.singleImage_flipImage);
        flipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.flipBitmap(bitmap, rotationArray[currentFlip]);
                originalBitmap = Tools.flipBitmap(originalBitmap, rotationArray[currentFlip]);
                currentFlip++;
                if (currentFlip == rotationArray.length)
                    currentFlip = 0;
                imageView.setImageBitmap(bitmap);
            }
        });

        //rotate
        rotateImage = (ImageView) findViewById(R.id.singleImage_rotateImage);
        rotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = Tools.rotateBitmap(bitmap);
                originalBitmap = Tools.rotateBitmap(originalBitmap);
                imageView.setImageBitmap(bitmap);
            }
        });

        colorFilter = (ImageView) findViewById(R.id.singleImage_colorFilter);
        colorFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(EditActivity.this);
                alert.setTitle("Image effect");
                alert.setItems(effectNames, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            bitmap = originalBitmap;
                        } else {
                            bitmap = Tools.changeColor(bitmap, effectsArray[which]);
                        }
                        imageView.setImageBitmap(bitmap);

                    }
                });
                alert.show();
            }
        });

        seekLayout = (LinearLayout) findViewById(R.id.singleImage_seekLayout);
        seekLayout.setVisibility(View.INVISIBLE);

        contrastBar = (SeekBar) findViewById(R.id.singleImage_contrast);
        contrastBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeContrast(originalBitmap, contrastBar.getProgress());
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });

        brightnessBar = (SeekBar) findViewById(R.id.singleImage_brigthness);
        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeBrightness(originalBitmap, brightnessBar.getProgress() - 255);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });

        saturationBar = (SeekBar) findViewById(R.id.singleImage_saturation);
        saturationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                bitmap = Tools.changeSaturation(originalBitmap, saturationBar.getProgress() - 100);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });

        //rotate
        seekControl = (ImageView) findViewById(R.id.singleImage_seekControl);
        seekControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (seekLayout.getVisibility() == View.INVISIBLE) {
                    seekLayout.setVisibility(View.VISIBLE);
                } else {
                    seekLayout.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private boolean isInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) EditActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        } else {
            return true;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 777) {
            Bundle extras = data.getExtras();
            addText((String) extras.get("text"), (String) extras.get("font"), (int) extras.get("color"), (int) extras.get("outline"));
        }
    }

    private void addText(String text, String font, int color, int outline) {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        FontPreview ft = new FontPreview(EditActivity.this, text, typeface, color, outline);
        ft.setOnTouchListener(mover);
        mainLayout.addView(ft);
    }

    private View.OnTouchListener mover = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            FontPreview font = (FontPreview) v;
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:

                    font.setX(event.getRawX() + font.holdModifierX);
                    font.setY(event.getRawY() + font.holdModifierY);

                    break;

                case MotionEvent.ACTION_DOWN:

                    font.holdModifierX = font.getX() - event.getRawX();
                    font.holdModifierY = font.getY() - event.getRawY();

                    break;
                case MotionEvent.ACTION_UP:

                    font.holdModifierX = 0;
                    font.holdModifierY = 0;

                    break;

                default:
                    break;

            }
            return true;
        }
    };
}
